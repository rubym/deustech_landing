import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { SubscribeComponent } from './subscribe/subscribe.component';
import { FooterContactsComponent } from './footer-contacts/footer-contacts.component';
import { CopyrightSectionComponent } from './copyright-section/copyright-section.component';
import { TopSectionComponent } from './top-section/top-section.component';
import { StepsComponent } from './steps/steps.component';
import { AboutComponent } from './about/about.component';

@NgModule({
  declarations: [
    AppComponent,
    SubscribeComponent,
    FooterContactsComponent,
    CopyrightSectionComponent,
    TopSectionComponent,
    StepsComponent,
    AboutComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
