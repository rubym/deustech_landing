import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'abe-copyright-section',
  templateUrl: './copyright-section.component.html',
  styleUrls: ['./copyright-section.component.scss']
})
export class CopyrightSectionComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
